/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "stdint.h"
#include "HightData.h"
#include "wit_c_sdk.h"
#include "stdbool.h"
#include "usart_msg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct
{
    uint8_t adress;
    uint16_t C[6];    //PROM
    uint16_t reserve;
    uint16_t crc;
    uint32_t D[2];    //D1 temperature data & D2 pressure data
    int32_t dT;    //Difference between actual and reference temperature
    int64_t OFF;    //Offset at actual temperature
    int64_t SENS;    //Sensitivity at actual temperature
    int32_t TEMP_MS;    //Actual temperature
    int32_t P;    //Actual pressure
}MS5611_t;

typedef struct {
    float x;  // ???
    float P;  // ?????
    float Q;  // ???????
    float R;  // ???????
} KalmanFilter;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MS5611_OK 0x01
#define MS5611_ERROR 0x00
//MS5611 ADDR CSB
#define MS5611_SLAVE_ADDR 0XEE

//COMMAND
#define MS5611_CMD_REST 0X1E 
#define MS5611_CMD_CONVERT_D1_256 0X40
#define MS5611_CMD_CONVERT_D1_512 0X42
#define MS5611_CMD_CONVERT_D1_1024 0X44
#define MS5611_CMD_CONVERT_D1_2048 0X46
#define MS5611_CMD_CONVERT_D1_4096 0X48
#define MS5611_CMD_CONVERT_D2_256 0X50
#define MS5611_CMD_CONVERT_D2_512 0X52
#define MS5611_CMD_CONVERT_D2_1024 0X54
#define MS5611_CMD_CONVERT_D2_2048 0X56
#define MS5611_CMD_CONVERT_D2_4096 0X58

#define MS6511_ADC_READ 0X00

#define MS5611_PROM_READ_0 0XA0
#define MS5611_PROM_READ_1 0XA2
#define MS5611_PROM_READ_2 0XA4
#define MS5611_PROM_READ_3 0XA6
#define MS5611_PROM_READ_4 0XA8
#define MS5611_PROM_READ_5 0XAA
#define MS5611_PROM_READ_6 0XAC
#define MS5611_PROM_READ_7 0XAE



#define ACC_UPDATE         0x01
#define GYRO_UPDATE        0x02
#define ANGLE_UPDATE    	 0x04
#define MAG_UPDATE         0x08
#define READ_UPDATE        0x80


#define LED_ON  0x01
#define LED_OFF 0x00



#define BOOM_PIN					 GPIO_PIN_5
#define BOOM_GPIO_PORT     GPIOC
#define BOOM_GPIO_CLK_ENABLE() __GPIOC_CLK_ENABLE()
#define BOOM_LED(a)    HAL_GPIO_WritePin(BOOM_GPIO_PORT,BOOM_PIN,a)


#define MPU6050_PIN					 GPIO_PIN_1
#define MPU6050_GPIO_PORT    GPIOA
#define MPU6050_GPIO_CLK_ENABLE() __GPIOC_CLK_ENABLE()
#define MPU6050_LED(a)    HAL_GPIO_WritePin(MPU6050_GPIO_PORT,MPU6050_PIN,a)


#define MS5611_PIN					 GPIO_PIN_5
#define MS5611_GPIO_PORT     GPIOB
#define MS5611_GPIO_CLK_ENABLE() __GPIOC_CLK_ENABLE()
#define MS5611_LED(a)    HAL_GPIO_WritePin(MS5611_GPIO_PORT,MS5611_PIN,a)


static char s_cDataUpdate = 0;
static void AutoScanSensor(void);
static void SensorUartSend(uint8_t *p_data, uint32_t uiSize);
static void CopeSensorData(uint32_t uiReg, uint32_t uiRegNum);



/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
void MS5611_Rest(I2C_HandleTypeDef* I2Cx);
uint8_t MS5611_init(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct);
uint8_t MS5611_PROM_read(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct);
uint8_t MS5611_read_temp(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution);
uint8_t NB_MS5611_request_temp(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution);
uint8_t NB_MS5611_pull_temp(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct);
uint8_t MS5611_read_press (I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution);
uint8_t NB_MS5611_request_press (I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution);
uint8_t NB_MS5611_pull_press (I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct);
uint8_t MS5611_calculate(MS5611_t* datastruct);
double mbar_to_height(double pressure_mbar) ;

void kalman_init(KalmanFilter *kf, float initial_x, float initial_P, float process_noise, float measurement_noise);
void kalman_predict(KalmanFilter *kf, float control_input);
void kalman_update(KalmanFilter *kf, float measurement);

bool usart_send_message_ubox(uint8_t function_id,uint16_t value[],int length);

static float fAcc[3], fGyro[3], fAngle[3], fYaw;//MPU6050的数据

static void usart1_send_char(uint8_t c);
static void usart1_niming_report(uint8_t fun,uint8_t *data,uint8_t len);
static void usart1_report_imu(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz,short roll,short pitch,short yaw);

int fputc(int ch, FILE *file)
{
    HAL_UART_Transmit(&huart2, (uint8_t*)&ch, 1, 1);
    return ch;
}
uint32_t uiBuad = 9600;
uint8_t ucRxData = 0;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart->Instance==USART2)
  {
      WitSerialDataIn(ucRxData);
      UART_Start_Receive_IT(huart, &ucRxData, 1);
  }
}

void LEDInit(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
	
	
	
	usart_frame_init();
	
	
	
	
	MS5611_t ms5611_t;
	uint8_t result= MS5611_init(&hi2c1,&ms5611_t);
	if(result == MS5611_OK){
		char *str="MS5611_init is Success!";
		HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
	}
	else{
		char *str="MS5611_init is Error!";
		HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
	}
	HAL_Delay(1000);
	
	

	result=MS5611_PROM_read(&hi2c1,&ms5611_t);
	if(result == MS5611_OK){
		char *str="MS5611_PROM_read is Success!\n";
		HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
	}
	else{
		char *str="MS5611_PROM_read is Error!\n";
		HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
	}
	HAL_Delay(1000);
	
	KalmanFilter kf;
  kalman_init(&kf, 1012.0, 1.0, 0.005, 0.1);
	
	init_data();//初始化高度数
	
	
	//init JY60 code
	
	
	int i=0;
	WitInit(WIT_PROTOCOL_NORMAL, 0x50);
	WitSerialWriteRegister(SensorUartSend);
	WitRegisterCallBack(CopeSensorData);
	
	//printf("\r\n********************** wit-motion normal example  ************************\r\n");
	AutoScanSensor();
	
	
	//LED灯初始化
	LEDInit();
	
	

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		
			MS5611_LED(LED_ON);
			char *str="  ";
		
			kalman_predict(&kf, 0.0);
		
			
			result=MS5611_read_press(&hi2c1,&ms5611_t,MS5611_CMD_CONVERT_D2_4096);
			
				
			result=MS5611_read_temp(&hi2c1,&ms5611_t,MS5611_CMD_CONVERT_D2_4096);

	
			result=MS5611_calculate(&ms5611_t);
		
			kalman_update(&kf, ((double)ms5611_t.P/100));

			int kx=kf.x*1000;
			MS5611_LED(LED_OFF);

      if(s_cDataUpdate)
      {
            MPU6050_LED(LED_ON);
						HAL_Delay(50);
						
            for(i = 0; i < 3; i++)
            {
                fAcc[i] = sReg[AX+i] / 32768.0f * 16.0f;
                fGyro[i] = sReg[GX+i] / 32768.0f * 2000.0f;
                fAngle[i] = sReg[Roll+i] / 32768.0f * 180.0f;
            }
            if(s_cDataUpdate & ACC_UPDATE)
            {
                //printf("acc:%.3f %.3f %.3f\r\n", fAcc[0], fAcc[1], fAcc[2]);
                s_cDataUpdate &= ~ACC_UPDATE;
							            }
            if(s_cDataUpdate & GYRO_UPDATE)
            {
                //printf("gyro:%.3f %.3f %.3f\r\n", fGyro[0], fGyro[1], fGyro[2]);
                s_cDataUpdate &= ~GYRO_UPDATE;
            }
            if(s_cDataUpdate & ANGLE_UPDATE)
            {
                fYaw = (float)((unsigned short)sReg[Yaw]) / 32768 * 180.0;
                //printf("angle:%.3f %.3f %.3f(%.3f)\r\n", fAngle[0], fAngle[1], fAngle[2], fYaw);
                s_cDataUpdate &= ~ANGLE_UPDATE;
            }
            if(s_cDataUpdate & MAG_UPDATE)
            {
                //printf("mag:%d %d %d\r\n", sReg[HX], sReg[HY], sReg[HZ]);
                s_cDataUpdate &= ~MAG_UPDATE;
            }
            s_cDataUpdate = 0;
						
						MPU6050_LED(LED_OFF);
						
						
						
						usart_frame_reset(&send_frame_struct);	
						uint16_t data[]={(uint16_t)(32768+fAcc[0]*100),(uint16_t)(32768+fAcc[1]*100),(uint16_t)(32768+fAcc[2]*100),(uint16_t)(32768+fGyro[0]),(uint16_t)(32768+fGyro[1])
							,(uint16_t)(32768+fGyro[2]),(uint16_t)(32768+fAngle[0]),(uint16_t)(32768+fAngle[1]*100),(uint16_t)(32768+fAngle[2]*100),(uint16_t)(32768+hights[1013560-kx]*100)
						,(uint16_t)(32768+(double)ms5611_t.TEMP_MS)};
						usart_send_message_ubox(0x01,data,11);
			

						//usart1_report_imu(fAcc[0],fAcc[1],fAcc[2],fGyro[0],fGyro[1],fGyro[2],fAngle[0]*100,fAngle[1]*100,0);
						
						
						
      }
			HAL_Delay(100); 
				
			
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */
	
  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */
	UART_Start_Receive_IT(&huart2, &ucRxData, 1);
  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
/*
 * Function for reseting the sensor
 */
void MS5611_Rest(I2C_HandleTypeDef* I2Cx)
{
    uint8_t RESET = (uint8_t)MS5611_CMD_REST;
    HAL_I2C_Master_Transmit(I2Cx, MS5611_SLAVE_ADDR, &RESET, 1, 1000);
    HAL_Delay(4);
}
/*
 * Function for reading PROM memories of the sensor
 */
uint8_t MS5611_PROM_read(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct){

    uint8_t i;
    uint8_t data[2];
    uint8_t PROM[8] = {MS5611_PROM_READ_0,
            MS5611_PROM_READ_1,
            MS5611_PROM_READ_2,
            MS5611_PROM_READ_3,
            MS5611_PROM_READ_4,
            MS5611_PROM_READ_5,
            MS5611_PROM_READ_6,
            MS5611_PROM_READ_7
    };
    I2C_HandleTypeDef* Handle = I2Cx;
    uint8_t address = datastruct->adress;

    while(HAL_I2C_Master_Transmit(Handle, address, &PROM[0], 1, 100) != HAL_OK);

    while(HAL_I2C_Master_Receive(Handle, address, data, 2, 100) != HAL_OK);

    datastruct->reserve = (uint16_t)(data[0] << 8 | data[1]);

    for (i=1;i<=6;i++){

        while(HAL_I2C_Master_Transmit(Handle, address, &PROM[i], 1, 100) != HAL_OK);

        while(HAL_I2C_Master_Receive(Handle, address, data, 2, 100) != HAL_OK);

        datastruct->C[i-1] = (uint16_t )(data[0] << 8 | data[1]);
    }

    while(HAL_I2C_Master_Transmit(Handle, address, &PROM[7], 1, 100) != HAL_OK);

    while(HAL_I2C_Master_Receive(Handle, address, data, 2, 100) != HAL_OK);

    datastruct->crc = (uint16_t)(data[0] << 8 | data[1]);

    return MS5611_OK;
}
uint8_t MS5611_init(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct)
{
    MS5611_Rest(I2Cx);
    datastruct->adress = MS5611_SLAVE_ADDR; //add slave adress to the datastruct
    MS5611_PROM_read(I2Cx,datastruct);
    return MS5611_OK;
}

/*
 * Function for reading raw temperature of the sensor
 */
uint8_t MS5611_read_temp(I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution)
{
    I2C_HandleTypeDef* Handle = I2Cx;
    uint8_t address = datastruct->adress;
    uint8_t reg = MS6511_ADC_READ;
    uint8_t data[5];
    uint8_t cmd;
    uint8_t conv_T;

    switch(resolution){
    case MS5611_CMD_CONVERT_D2_256 : cmd = MS5611_CMD_CONVERT_D2_256; conv_T = 1;
    break;
    case MS5611_CMD_CONVERT_D2_512 : cmd = MS5611_CMD_CONVERT_D2_512; conv_T = 2;
    break;
    case MS5611_CMD_CONVERT_D2_1024 : cmd = MS5611_CMD_CONVERT_D2_1024; conv_T = 3;
    break;
    case MS5611_CMD_CONVERT_D2_2048 : cmd = MS5611_CMD_CONVERT_D2_2048; conv_T = 5;
    break;
    case MS5611_CMD_CONVERT_D2_4096 : cmd = MS5611_CMD_CONVERT_D2_4096; conv_T = 9;
    break;
    default : cmd = MS5611_CMD_CONVERT_D2_4096; conv_T = 9;
    }

    while(HAL_I2C_Master_Transmit(Handle, address, &cmd, 1, 100) != HAL_OK);//asking adc to store data
    HAL_Delay(20);                                                         //convertion time
    while(HAL_I2C_Master_Transmit(Handle, address, &reg, 1, 100) != HAL_OK);//asking for the data
    while(HAL_I2C_Master_Receive(Handle, address, data, 3, 100) != HAL_OK);//receive the data

    datastruct->D[1] = (data[0] << 16 | data[1] << 8 | data[2]);
    return MS5611_OK;
}

/*
 * Function for reading raw pressure of the sensor
 */
uint8_t MS5611_read_press (I2C_HandleTypeDef* I2Cx, MS5611_t* datastruct, uint8_t resolution)
{
    I2C_HandleTypeDef* Handle = I2Cx;
    uint8_t address = datastruct->adress;
    uint8_t reg = MS6511_ADC_READ;
    uint8_t data[3];
    uint8_t cmd;
    uint8_t conv_T;

    switch(resolution){
    case MS5611_CMD_CONVERT_D1_256 : cmd = MS5611_CMD_CONVERT_D1_256; conv_T = 1;
    break;
    case MS5611_CMD_CONVERT_D1_512 : cmd = MS5611_CMD_CONVERT_D1_512; conv_T = 2;
    break;
    case MS5611_CMD_CONVERT_D1_1024 : cmd = MS5611_CMD_CONVERT_D1_1024; conv_T = 3;
    break;
    case MS5611_CMD_CONVERT_D1_2048 : cmd = MS5611_CMD_CONVERT_D1_2048; conv_T = 5;
    break;
    case MS5611_CMD_CONVERT_D1_4096 : cmd = MS5611_CMD_CONVERT_D1_4096; conv_T = 9;
    break;
    default : cmd = MS5611_CMD_CONVERT_D1_4096; conv_T = 9;
    }

    while(HAL_I2C_Master_Transmit(Handle, address, &cmd, 1, 100) != HAL_OK);//asking adc to store data
    HAL_Delay(20);                                                         //convertion time
    while(HAL_I2C_Master_Transmit(Handle, address, &reg, 1, 100) != HAL_OK);//asking for the data
    while(HAL_I2C_Master_Receive(Handle, address, data, 3, 100) != HAL_OK);//receive the data

    datastruct->D[0] = (data[0] << 16 | data[1] << 8 | data[2]);
    return MS5611_OK;
}

/*
 * Function for pressure and temperature calculation
 */
uint8_t MS5611_calculate(MS5611_t* datastruct)
{
    int64_t dT = 0,TEMP_MS = 0,T2 = 0,OFF = 0,OFF2 = 0,SENS2 = 0,SENS = 0,PRES = 0;

    dT = datastruct->D[1] - ((int32_t) (datastruct->C[4])<<8);
    TEMP_MS = 2000 + ((int32_t) (dT*(datastruct->C[5]))>>23);
    OFF = (((int64_t)(datastruct->C[1])) << 16) + (((datastruct->C[3]) * dT) >> 7);
    SENS = (((int64_t)(datastruct->C[0])) << 15) + (((datastruct->C[2]) * dT) >> 8);

    if(TEMP < 2000) { //temperature < 20�C
        T2 = ( dT*dT )>>31;
        OFF2 = 5 * (TEMP - 2000) * (TEMP - 2000) / 2;
        SENS2 = 5 * (TEMP - 2000) * (TEMP - 2000) / 4;

        if (TEMP < -1500) { //temperature < -15�C
            OFF2 = OFF2 + (7 * (TEMP + 1500) * (TEMP + 1500));
            SENS2 = SENS2 + (11 * (TEMP + 1500) * (TEMP + 1500) / 2);
        }
    }
    else { //temperature > 20�C
        T2 = 0;
        OFF2 = 0;
        SENS2 = 0;
    }

    datastruct->dT = dT;
    datastruct->OFF = OFF - OFF2;
    datastruct->TEMP_MS = TEMP - T2;
    datastruct->SENS = SENS - SENS2;

    PRES = ((((int32_t)(datastruct->D[0]) * (datastruct->SENS))>>21) - (datastruct->OFF))>>15;
    datastruct->P = PRES;
    return MS5611_OK;
}




void kalman_init(KalmanFilter *kf, float initial_x, float initial_P, float process_noise, float measurement_noise) {
    kf->x = initial_x;
    kf->P = initial_P;
    kf->Q = process_noise;
    kf->R = measurement_noise;
}


// 卡尔曼滤波器预测步骤
void kalman_predict(KalmanFilter *kf, float control_input) {
    // 预测状�??
    kf->x += control_input;
    // 预测协方�?
    kf->P += kf->Q;
}

// 卡尔曼滤波器更新步骤
void kalman_update(KalmanFilter *kf, float measurement) {
    // 计算卡尔曼增�?
    float K = kf->P / (kf->P + kf->R);
    // 更新状�?�估计�??
    kf->x += K * (measurement - kf->x);
    // 更新协方差估计�??
    kf->P *= (1 - K);
}


double mbar_to_height(double pressure_mbar) 
{
	
	
	char press_km[120];
			
			
	
	const double P0 = 1013.25; // ?????,??:mbar
	double pows=powf(pressure_mbar /P0, 0.190263f);
	return 44330.8 * (1 - pows) / 1000;
}



static void SensorUartSend(uint8_t *p_data, uint32_t uiSize)
{
    HAL_UART_Transmit_IT(&huart2, p_data, uiSize);
}
static void CopeSensorData(uint32_t uiReg, uint32_t uiRegNum)
{
    int i;
    for(i = 0; i < uiRegNum; i++)
    {
        switch(uiReg)
        {
            case AZ:
                s_cDataUpdate |= ACC_UPDATE;
            break;
            case GZ:
                s_cDataUpdate |= GYRO_UPDATE;
            break;
            case HZ:
                s_cDataUpdate |= MAG_UPDATE;
            break;
            case Yaw:
                s_cDataUpdate |= ANGLE_UPDATE;
            break;
            default:
                s_cDataUpdate |= READ_UPDATE;
            break;
        }
        uiReg++;
    }
}

static void AutoScanSensor(void)
{
    const uint32_t c_uiBaud[9] = {4800, 9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600};
    int i, iRetry;
    
    for(i = 0; i < 9; i++)
    {
        uiBuad = c_uiBaud[i];
        MX_USART2_UART_Init();
        iRetry = 2;
        do
        {
            s_cDataUpdate = 0;
            WitReadReg(AX, 3);
            HAL_Delay(100);
            if(s_cDataUpdate != 0)
            {
							
							char *str="baud find sensor!";
							HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
              //printf("%d baud find sensor\r\n\r\n", c_uiBaud[i]);
              return ;
            }
            iRetry--;
        }while(iRetry);        
    }
		char *str="can not find sensor!";
		HAL_UART_Transmit_IT(&huart1,(uint8_t*)str,strlen(str));
    
}

static void usart1_report_imu(short aacx,short aacy,short aacz,short gyrox,short gyroy,short gyroz,short roll,short pitch,short yaw)
{
    
    
    uint8_t buf[28]={
    
    0x00};
    
    buf[0]=(aacx>>8)&0xFF;
    buf[1]=aacx&0xFF;
    buf[2]=(aacy>>8)&0xFF;
    buf[3]=aacy&0xFF;
    buf[4]=(aacz>>8)&0xFF;
    buf[5]=aacz&0xFF;
    
    buf[6]=(gyrox>>8)&0xFF;
    buf[7]=gyrox&0xFF;
    buf[8]=(gyroy>>8)&0xFF;
    buf[9]=gyroy&0xFF;
    buf[10]=(gyroz>>8)&0xFF;
    buf[11]=gyroz&0xFF;
    
    //12-17????,MPU6050?????,??0x00
    
    buf[18]=(roll>>8)&0xFF;
    buf[19]=roll&0xFF;
    buf[20]=(pitch>>8)&0xFF;
    buf[21]=pitch&0xFF;
    buf[22]=(yaw>>8)&0xFF;
    buf[23]=yaw&0xFF;
    
    //24-27???????0x00
    
    usart1_niming_report(0xAF,buf,28);
}

static void usart1_niming_report(uint8_t fun,uint8_t *data,uint8_t len)
{
    
    
    uint8_t send_buf[32]={
    
    0x00};
    uint8_t i;
    if(len>28) return;//??28???,??
    send_buf[len+3]=0;//?????
    send_buf[0]=0x88;//??0x88
    send_buf[1]=fun;//???FUN
    send_buf[2]=len;//?????LEN
    for(i=0;i<len;i++)
        send_buf[i+3]=data[i];
    for(i=0;i<len+3;i++)
        send_buf[len+3] += send_buf[i];//???????SUM
    for(i=0;i<len+4;i++)
        usart1_send_char(send_buf[i]);//???????1
}    

static void usart1_send_char(uint8_t c)
{
    
    
	//Uart1Send(&c,1);
	
	HAL_UART_Transmit_IT(&huart1,&c,1);
}

void LEDInit(void){
	GPIO_InitTypeDef GPIO_initStruct;
	BOOM_GPIO_CLK_ENABLE();
	GPIO_initStruct.Pin=BOOM_PIN;
	GPIO_initStruct.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_initStruct.Pull=GPIO_PULLDOWN;
	GPIO_initStruct.Speed=GPIO_SPEED_HIGH;
	HAL_GPIO_Init(BOOM_GPIO_PORT,&GPIO_initStruct);
	
	MPU6050_GPIO_CLK_ENABLE();
	GPIO_initStruct.Pin=MPU6050_PIN;
	GPIO_initStruct.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_initStruct.Pull=GPIO_PULLDOWN;
	GPIO_initStruct.Speed=GPIO_SPEED_HIGH;
	HAL_GPIO_Init(MPU6050_GPIO_PORT,&GPIO_initStruct);
	
	
		
	MS5611_GPIO_CLK_ENABLE();
	GPIO_initStruct.Pin=MS5611_PIN;
	GPIO_initStruct.Mode=GPIO_MODE_OUTPUT_PP;
	GPIO_initStruct.Pull=GPIO_PULLDOWN;
	GPIO_initStruct.Speed=GPIO_SPEED_HIGH;
	HAL_GPIO_Init(MS5611_GPIO_PORT,&GPIO_initStruct);
	
}

bool usart_send_message_ubox(uint8_t function_id,uint16_t value[],int length){
	    uint8_t i=0,cnt=0;
    uint8_t buff[46];
    memset(send_frame_struct.data,0,40);
    send_frame_struct.function_id=function_id;            //信息输出--字符�?+数字

    /*协议规定VAL在前，先对要求的32位数据进行截�?*/
//    send_frame_struct.data[cnt++]=BYTE0(value);
//    send_frame_struct.data[cnt++]=BYTE1(value);
//
//    send_frame_struct.data[cnt++]=BYTE2(value);
//    send_frame_struct.data[cnt++]=BYTE3(value);

    
    if(length>20) return false;
    for (int i = 0; i < length; ++i) {
        //小端存储�?
        send_frame_struct.data[cnt++]=BYTE0(value[i]);  //低字节位
        send_frame_struct.data[cnt++]=BYTE1(value[i]);  //高字节位
    }



    send_frame_struct.data_len=length*2;            //<记录下数据部分长�?

    usart_check_calculate(&send_frame_struct);    //<计算校验�?
    frame_turn_to_array(&send_frame_struct,buff); //<通信帧转线�?�数�?

    HAL_UART_Transmit(&huart1,buff,6+send_frame_struct.data_len,200);// 集成到上位机的MAIN函数里面
    return true;
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
