//
// Created by 李锋 on 2024/4/12.
//

#ifndef MPU6050_USART_MSG_H
#define MPU6050_USART_MSG_H

#include "stdio.h"
#include "stdint.h"
#include <string.h>


/****通信帧对象结构体****/
typedef struct
{
    uint8_t head;					 //<帧头
    uint8_t target_addr;			 //<目标址地
    uint8_t function_id;			 //<该帧要实现某功能的功能码id
    uint8_t data_len;				 //<数据长度
    uint8_t data[40];				 //<数据内容，协议最高只支持40字节数据
    uint8_t sum_check;				 //<和校验
    uint8_t add_check;				 //<附加校验

}UsartFrameStruct;

//目标地址宏定义
#define FRAME_HEADER 		  0XEE   //<帧头

#define GENERAL_OUTPUT_ADDR	  0XFF   //<目标址地 = 广播型输出
#define HOST_ADDR			  0XAF   //<目标址地 = 向上位机输出


//功能码定义部分
#define SENSORE_READ_DATA			  0X01   //<功能码id = 获取全部传感器数据，具体定义uint16类型（0-65535）【最多不能超过20个】
                                             //0-2位陀螺仪加速度x,y,z  3-5陀螺仪角度值x,y,z (陀螺仪有数据状态就是正常)
                                             //6 气压计的状态位  7气压计的高度值 （气压计有数据就是状态正常）
#define MSG_RS_DATA                   0XF0   //字符串传输






//32位数据进行四个字节剥离拆分，从低位到高位
#define BYTE0(temp)	   (*(char*)(&temp))
#define BYTE1(temp)	   (*((char*)(&temp)+1))
//#define BYTE2(temp)	   (*((char*)(&temp)+2))
//#define BYTE3(temp)	   (*((char*)(&temp)+3))

#define USART_BLACK         0X00  //<字符黑色打印
#define USART_RED           0X01  //<字符红色打印
#define USART_GREEN         0X02  //<字符绿色打印




void usart_frame_init(void);
void usart_frame_reset(UsartFrameStruct* frame);
void usart_check_calculate(UsartFrameStruct* frame);
uint8_t usart_check(UsartFrameStruct* frame);
void frame_turn_to_array(UsartFrameStruct* frame,uint8_t*str);

extern UsartFrameStruct send_frame_struct,rec_frame_struct;

#endif //MPU6050_USART_MSG_H
